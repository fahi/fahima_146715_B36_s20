<?php
include_once("../../vendor/autoload.php");

use App\person;
use App\student;

$objperson = new person();
$objperson->showPersonalInfo();

$objstudent= new student();
$objstudent->showStudentInfo();